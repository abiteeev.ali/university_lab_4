package org.example.university_lab_4.matrixFilling

import kotlin.math.pow
import kotlin.properties.Delegates

/**
 * Builder for our matrix
 */
class WeirdMatrixBuilder {
    private var counter: Int = 1
    private var dim by Delegates.notNull<Int>()
    private var branchNumber = 0
    private var row = 0
    private var column = 0
    private var i = 0
    private var j = 0
    private var sum = 0 // the sum of the elements of an arithmetic progression

    /**
     * Build body with length equal size, and calculate first sum of an arithmetic progression
     * @param dim dimension of our matrix
     */
    fun buildSize(dim: Int) {
        this.dim = dim
        sum = 2 * dim - 1
    }
    /**
     * Allocates a matrix cell based on the number of the current element and writes value there
     * @param matrix where we allocate cell
     */
    fun buildCell(matrix: Matrix) {
        if (counter > sum) {
            branchNumber++ // increment branch number if we got to the next branch
            sum += 2 * dim - 2 * (branchNumber) - 1 // recalculate the sum of the elements of an arithmetic progression
        }
        // We need this if to avoid looping to find the branchNumber
        // Formula counter = dim +(-1)^n * column + (-1)^(n+1) * row + 2nk - n^2 - n;
        // as n = branchNumber = dim - max(column, row) - 1
        j = dim - branchNumber - 1
        i = (counter - dim + (-1).toDouble().pow(branchNumber + 1).toInt() * j - 2 * branchNumber * dim + branchNumber * (branchNumber + 1)) * (-1).toDouble().pow(branchNumber + 1).toInt()
        // We dont know j is row or column and this is why we need in this if
        if (dim - branchNumber - 1 >= i) {
            row = i
            column = j
        } else {
            row = j
            column = (counter - dim + (-1).toDouble().pow(branchNumber).toInt() * j - 2 * branchNumber * dim + branchNumber * (branchNumber + 1)) * (-1).toDouble().pow(branchNumber).toInt()
        }

        matrix.set(row, column, counter)
        counter++
    }
}
