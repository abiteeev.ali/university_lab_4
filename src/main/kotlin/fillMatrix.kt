package org.example.university_lab_4.matrixFilling

import java.lang.IllegalArgumentException

/**
 * Matrix filling according rule.
 * Changes matrix example of changed matrix for dim 4x4:
 * 16, 15, 8, 7,
 * 13, 14, 9, 6,
 * 12, 11, 10, 5,
 * 1, 2, 3, 4
 * @param matrix - quad matrix of Ints
 */
fun fillMatrix(matrix: Matrix, dim: Int) {
    if (dim <= 0) {
        throw IllegalArgumentException("Wrong dimension: it should be greater than 0")
    }
    val resultBuilder = WeirdMatrixBuilder()
    resultBuilder.buildSize(dim)
    (1 until dim * dim + 1).forEach() {
        resultBuilder.buildCell(matrix)
    }
}
