package org.example.university_lab_4.matrixFilling

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class TestMatrixFilling {

    @Test
    fun `Dim 0`() {
        var m = mock(Matrix::class.java)
        try {
            fillMatrix(m, 0)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Dim lower than 0`() {
        var m = mock(Matrix::class.java)
        try {
            fillMatrix(m, -1)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Matrix 1 x 1`() {
        var m = mock(Matrix::class.java)

        fillMatrix(m, 1)

        Mockito.verify(m).set(0, 0, 1)
    }

    @Test
    fun `Matrix 4 x 4`() {
        var m = mock(Matrix::class.java)

        fillMatrix(m, 4)
        //   0   1   2  3
        // 0 16, 15,  8, 7,
        // 1 13, 14,  9, 6,
        // 2 12, 11, 10, 5,
        // 3  1,  2,  3, 4
        Mockito.verify(m).set(3, 0, 1)
        Mockito.verify(m).set(3, 1, 2)
        Mockito.verify(m).set(3, 2, 3)
        Mockito.verify(m).set(3, 3, 4)

        Mockito.verify(m).set(2, 3, 5)
        Mockito.verify(m).set(1, 3, 6)
        Mockito.verify(m).set(0, 3, 7)

        Mockito.verify(m).set(0, 2, 8)
        Mockito.verify(m).set(1, 2, 9)
        Mockito.verify(m).set(2, 2, 10)

        Mockito.verify(m).set(2, 1, 11)
        Mockito.verify(m).set(2, 0, 12)

        Mockito.verify(m).set(1, 0, 13)
        Mockito.verify(m).set(1, 1, 14)

        Mockito.verify(m).set(0, 1, 15)
        Mockito.verify(m).set(0, 0, 16)
    }

    @Test
    fun `Matrix 3 x 3`() {
        var m = mock(Matrix::class.java)

        fillMatrix(m, 3)
        //    0   1   2
        // 0  9,  6,  5,
        // 1  8,  7,  4,
        // 2  1,  2,  3
        Mockito.verify(m).set(2, 0, 1)
        Mockito.verify(m).set(2, 1, 2)
        Mockito.verify(m).set(2, 2, 3)
        Mockito.verify(m).set(1, 2, 4)
        Mockito.verify(m).set(0, 2, 5)
        Mockito.verify(m).set(0, 1, 6)
        Mockito.verify(m).set(1, 1, 7)
        Mockito.verify(m).set(1, 0, 8)
        Mockito.verify(m).set(0, 0, 9)
    }
}
